import React from 'react'
import NewThreadForm from "../../../components/UI/form/newThreadForm/newThreadForm";
const NewThread = () =>{
    return (
        <NewThreadForm>
            <h1>Create new thread in <i>Cooking</i></h1>
        </NewThreadForm>
    )
}

export default NewThread
