import React, { Component } from "react";
import { Card, CardContent, Typography } from "@material-ui/core";

class DashboardForum extends Component {
  render() {
    return (
      <Card>
        <CardContent>
          <Typography color="textSecondary" gutterBottom>
            DashboardForum
          </Typography>
        </CardContent>
      </Card>
    );
  }
}

export default DashboardForum;
